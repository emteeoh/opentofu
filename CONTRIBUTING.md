# Contributing

Please read about CI/CD components and best practices at: https://docs.gitlab.com/ee/ci/components

## README

The [`README.md`](README.md) file is generated using `make docs` (see [`Makefile`](Makefile))
from [`.gitlab/README.md.template`](.gitlab/README.md.template).

## Upgrade OpenTofu versions

When adding new OpenTofu versions the following places need to be updated:

| File                                                         | What                                                                                      |
| ------------------------------------------------------------ | ----------------------------------------------------------------------------------------- |
| [`templates/full-pipeline.yml`](templates/full-pipeline.yml) | The `default` value and `options` list of the `sepc.inputs.opentofu_versions` entry.      |
| [`.gitlab-ci.yml`](.gitlab-ci.yml)                           | The `.opentofu_versions.parallel.matrix` list and the `LATEST_OPENTOFU_VERSION` variable. |

All of the above definitions have to match each other.
We currently need to change it in multiple places, because there is not a good way to share information
from the templates and the components pipeline defintion - at least in the features we'd like to use them.

## Backports

The OpenTofu CI/CD component needs to be backported as OpenTofu CI/CD template, 
because components are not yet properly supported in self-managed instances.
That is, they are not bundled and it's not possible to use a component across instances.

The OpenTofu CI/CD job and pipeline templates can be generated using `make backports`.
The output is generated into the `backports` folder. 
Please contribute those files only upon full manual inspection to the canonical GitLab repository.
