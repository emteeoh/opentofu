ARG BASE_IMAGE

FROM $BASE_IMAGE

RUN apk add --no-cache \
  curl \
  gcompat \
  git \
  idn2-utils \
  jq \
  openssh \
  cosign

# Install OpenTofu using the installer script in standalone mode
# see https://opentofu.org/docs/intro/install/standalone
# We may want to switch to installing manually from GitHub and verifying signature
ARG OPENTOFU_VERSION
RUN curl --proto '=https' --tlsv1.2 -fsSL https://get.opentofu.org/install-opentofu.sh -o install-opentofu.sh && \
    chmod +x install-opentofu.sh && \
    ./install-opentofu.sh --install-method standalone --opentofu-version "${OPENTOFU_VERSION}" && \
    rm ./install-opentofu.sh && \
    tofu --version

WORKDIR /

COPY src/gitlab-tofu.sh /usr/bin/gitlab-tofu
RUN chmod +x /usr/bin/gitlab-tofu

# Override ENTRYPOINT
ENTRYPOINT []
